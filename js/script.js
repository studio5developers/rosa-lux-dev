if (typeof Object.assign != 'function') {
    // Must be writable: true, enumerable: false, configurable: true
    Object.defineProperty(Object, "assign", {
        value: function assign(target, varArgs) { // .length of function is 2
            'use strict';
            if (target == null) { // TypeError if undefined or null
                throw new TypeError('Cannot convert undefined or null to object');
            }

            var to = Object(target);

            for (var index = 1; index < arguments.length; index++) {
                var nextSource = arguments[index];

                if (nextSource != null) { // Skip over if undefined or null
                    for (var nextKey in nextSource) {
                        // Avoid bugs when hasOwnProperty is shadowed
                        if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
                            to[nextKey] = nextSource[nextKey];
                        }
                    }
                }
            }
            return to;
        },
        writable: true,
        configurable: true
    });
}

/** Add pre-loader before page load **/
jQuery(document).ready(function () {
    $('#preloader').fadeOut('50ms', function () {
        $('#preloader').remove();
    });
});

/** Add bootstrap active menu classes **/
jQuery(document).ready(function () {
    $("#menuLinks li a").on("click", function () {
        // find active menu
        $("#menuLinks")
            .find(".active")
            .removeClass("active");
        // add active class
        $(this)
            .parent()
            .addClass("active");
    });
});

/** add support for scrollbar on top **/
jQuery(document).ready(function () {
    $(window).on('scroll', function () {
        var docHeight = $(document).height(),
            winHeight = $(window).height(),
            viewport = docHeight - winHeight,
            positionY = $(window).scrollTop(),
            indicator = (positionY / (viewport)) * 100;
        $('.scroll-bar').css('width', indicator + '%');
    });
});

/** script for shuffle js on homepage **/
jQuery(document).ready(function () {
    // Shuffle js
    var Shuffle = window.Shuffle,
        element = document.querySelector('.shuffle-container'),
        shuffleInstance = new Shuffle(element, {
        itemSelector: '.picture-item'
    });
// shuffleInstance.filter('animal');
    $("#all").on("click", function () {
        shuffleInstance.filter();
    });
    $("#btnDemocracy").on("click", function () {
        shuffleInstance.filter('democracy');
    });
    $("#btnHistory").on("click", function () {
        shuffleInstance.filter('history');
    });
    $("#btnLabour").on("click", function () {
        shuffleInstance.filter('labour');
    });

    $("#btnSustainable").on("click", function () {
        shuffleInstance.filter('sustainable');
    });
    $("#btnFood").on("click", function () {
        shuffleInstance.filter('food');
    });
});

/** script for read all function on homepage **/
jQuery(document).ready(function () {
    $("#btnReadAll").attr('data-click-state', 0);
    $('.the-team').each(function () {
        $(this).find('.aspect__inner').addClass('toggleAllTeamMembersInfo');
    });
    //$("#btnReadAll").on("click", function () {
    //    if ($(this).attr('data-click-state') == 0) {
    //        $(this).attr('data-click-state', 1)
    //        $('.the-team').each(function () {
    //            $(this).find('.aspect__inner').addClass('toggleAllTeamMembersInfo');
    //        });
    //    }
    //    else {
    //        $(this).attr('data-click-state', 0)
    //
    //        $('.the-team').each(function () {
    //            $(this).find('.aspect__inner').removeClass('toggleAllTeamMembersInfo');
    //        });
    //    }
    //});
});

/** function for SVG address animations **/
jQuery(document).ready(function () {
    $.fn.showCountryBlocks = function (countryId) {
        $(this).hover(function () {
            var country = this;
            $(countryId).toggleClass('show');
            $(countryId).hover(function () {
                $(countryId).addClass('show');
                $(country).addClass('fill');
                $(countryId).mouseleave(function () {
                    $(countryId).removeClass('show');
                    $(country).removeClass('fill');
                })
            });
            return this;
        });
    };
});

/** Id calls for each country hover **/
jQuery(document).ready(function () {
    // Russia
    $("#russia").showCountryBlocks("#toggleRussia");
    $("#brazil").showCountryBlocks("#toggleBrazil");
    $("#venenzuela").showCountryBlocks("#toggleVenezuela");
    $("#equador").showCountryBlocks("#toggleEquador");
    $("#peru").showCountryBlocks("#togglePeru");
    $("#cuba").showCountryBlocks("#toggleCuba");
    $("#mexico").showCountryBlocks("#toggleMexico");
    $("#centralAmerica").showCountryBlocks("#toggleMexico");
    $("#usa").showCountryBlocks("#toggleUsa");
    $("#india").showCountryBlocks("#toggleIndia");
    $("#china").showCountryBlocks("#toggleChina");
    $("#vietnam").showCountryBlocks("#toggleVietnam");
    $("#turkey").showCountryBlocks("#toggleTurkey");
    $("#poland").showCountryBlocks("#togglePoland");
    $("#germany").showCountryBlocks("#toggleGermany");
    $("#serbia").showCountryBlocks("#toggleSerbia");
    $("#tanzania").showCountryBlocks("#toggleTanzania");
    $("#dakar").showCountryBlocks("#toggleDakar");
    $("#rsa").showCountryBlocks("#toggleSafrica");
    $("#belgium").showCountryBlocks("#toggleBelgium");
    $("#israel").showCountryBlocks("#toggleIsrael");
});

/** Function to pass into toggleable text below **/
$.fn.toggleText = function (t1, t2) {
    if (this.text() == t1) {
        this.text(t2);
    }
    else {
        this.text(t1);
    }
    return this;
};

/** Function to toggle view all countries in about page **/
jQuery(document).ready(function () {
    $('#toggleCountriesAll').click(function () {
        $(this).toggleText('Close', 'View full list');
        $(this).toggleClass('active');
        $('#toggleCountriesOverlay').toggleClass('open');
    });
});

new WOW().init();

///** Make logo sticky after scroll bottom **/
//$(document).ready(function () {
//
//    $(window).scroll(function () {
//        var distanceFromTop = $(document).scrollTop();
//        var homepages = ['/', 'index.html', 'index.htm', 'index.php', 'main.html', 'main.htm', 'home.html', 'home.htm'];
//
//        if (homepages.indexOf(window.location.pathname) >= 0) {
//            if (distanceFromTop >= $('#BrandRedBlock').height() + 100) {
//                $('#brandLogo').fadeIn(250).addClass('fixed');
//            }
//            else {
//                $('#brandLogo').fadeOut(0).removeClass('fixed');
//            }
//        }
//        else {
//            return null;
//        }
//    });
//});

/** Function to make images on about change colour **/
//$(document).ready(function () {
//    //project cooperation
//    var ctaPC = "#About1PC";
//    var ctaPCtoClr = $(ctaPC).find('img').attr("src");
//    var ctaPCtoGray = ctaPCtoClr.replace("BoyBW.jpg", "boycolor.jpg");
//
//    $(ctaPC).mouseover(function () {
//        $(this).find('img').fadeIn("50ms", function () {
//            $(this).attr("src", ctaPCtoGray);
//        });
//    })
//        .mouseout(function () {
//            $(this).find('img').fadeIn("50ms", function () {
//                $(this).attr("src", ctaPCtoClr);
//            });
//        });
//});
//
//$(document).ready(function () {
//// about rosa
//    var ctaRL = "#About1RL";
//    var ctaRLtoClr = $(ctaRL).find('img').attr("src");
//    var ctaRLtoGray = ctaRLtoClr.replace("bridgebw.jpg", "bridgecolor.jpg");
//    $(ctaRL).mouseover(function () {
//        $(this).find('img').fadeIn("50ms", function () {
//            $(this).attr("src", ctaRLtoGray);
//        });
//    })
//        .mouseout(function () {
//            $(this).find('img').fadeIn("50ms", function () {
//                $(this).attr("src", ctaRLtoClr);
//            });
//        });
//});

/** Toggle buttons close on click **/
$(document).ready(function () {
    //partners hover change image
    $('#accordion .ButtonThing').hover(function () {
        $(this).on("click", function () {
            $(this).parent().find("span").toggleClass("fa-chevron-down fa-chevron-up");
        });
    })
});

/** Read Cookie Function **/
$(document).ready(function () {
    var Exists = document.cookie.indexOf('RLPrivacy');
    $('#PrivacyPopup a').on('click', function () {
        $('#PrivacyPopup').hide();
    });
    if (Exists === 0) {
        $('#PrivacyPopup').hide();
    } else {
        $('#PrivacyPopup').show();
    }
});

/** Create Cookie Function **/
$(document).ready(function () {
    var when = new Date(Date.now());
    var date = new Date();
    var tomorrow = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 23, 59, 59);
    document.cookie = "RLPrivacy= " + when + "; expires=" + tomorrow;
});

/** Accordion mobile **/
$(document).ready(function () {
    if (window.matchMedia('(max-width: 992px)').matches) {
        $('#accordion .collapse').attr('data-parent', '');
    }
});


/** Shuffle JS Styles for Internet Explorer 10 and above**/
// IF THE BROWSER IS INTERNET EXPLORER 10
if (navigator.appVersion.indexOf("MSIE 10") !== -1) {
    $(document).ready(function () {
        $('.card-with-image img').each(function () {
            var t = $(this),
                s = 'url(' + t.attr('src') + ')',
                p = t.parent();
            t.hide();

            if (p.has('img')) {
                p.css({
                    'height': 504,
                    'background-size': 'cover',
                    'background-repeat': 'no-repeat',
                    'background-position': 'center',
                    'background-image': s
                });
            }
        });
    });
}

// IF THE BROWSER IS INTERNET EXPLORER 11
var UAString = navigator.userAgent;
if (UAString.indexOf("Trident") !== -1 && UAString.indexOf("rv:11") !== -1) {
    $(document).ready(function () {
        $('.card-with-image img').each(function () {
            var t = $(this),
                s = 'url(' + t.attr('src') + ')',
                p = t.parent();
            t.hide();

            if (p.has('img')) {
                p.css({
                    'height': 504,
                    'background-size': 'cover',
                    'background-repeat': 'no-repeat',
                    'background-position': 'center top',
                    'background-image': s
                });
            }
        });
    });
}

// Calendar slideshow
$('.multiple-items').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 4
});